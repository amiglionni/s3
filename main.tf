provider "aws" {
  version = "~> 2.0"
  region  = var.region
}

resource "aws_s3_bucket" "s3_without_versioning" {
  bucket = "bucket-aprendanuvem"
  acl    = "private"
  count = "${var.enable_versioning == false ? 1 : 0}"
  versioning {
    enabled = false
  }
}

resource "aws_s3_bucket" "s3_with_versioning" {
  bucket = "bucket-aprendanuvem"
  acl    = "private"
  count = "${var.enable_versioning == true ? 1 : 0}"
  versioning {
    enabled = true
  }
}